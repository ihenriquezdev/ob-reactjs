/**
 * Ejemplo Hooks:
 * - useState()
 * - useContext()
 */

import React, { useState, useContext } from 'react';

/**
 *
 * @returns Componente1
 * Dispone de un contexto que va a tener un valor que recibe
 * desde el padre
 */

const miContexto = React.createContext(null);

const Componente1 = () => {
  // Se inicializa el estado en null que va a rellenarse con
  // los datos del contexto del padre
  const state = useContext(miContexto);

  return (
    <div>
      <h1>El token es: {state.token}</h1>
      {/* Componente 2 */}
      <Componente2 />
    </div>
  );
};

const Componente2 = () => {
  const state = useContext(miContexto);
  return (
    <div>
      <h2>La session es: {state.session}</h2>
    </div>
  );
};

const ComponenteConContexto = () => {
  const estadoInicial = {
    token: '1234567',
    session: 1
  };

  // Creamos el estado de este componente
  const [sessionData, setSessionData] = useState(estadoInicial);
  function actualizarSession() {
    setSessionData((sessionData) => ({
      token: 'JWT123456789',
      session: ++sessionData.session
    }));
  }

  return (
    <div>
      <miContexto.Provider value={sessionData}>
        {/* Todo lo que este aqui dentro puede leer los datos de sessionData */}
        {/* Si se actualiza, los componentes de aqui tambien lo actualizan */}
        <h1>**** Ejemplo de useState y useContext</h1>
        <Componente1></Componente1>
        <button onClick={actualizarSession}>Actualizar session</button>
      </miContexto.Provider>
    </div>
  );
};

export default ComponenteConContexto;
