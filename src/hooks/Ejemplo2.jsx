/**
 * Ejemplo de uso de:
 * - useState()
 * - useRef()
 * - useEffect()
 */

import React, { useState, useEffect, useRef } from 'react';

const Ejemplo2 = () => {
  // Dos contadores distintos, cada uno con un estado diferente
  const [contador1, setContador1] = useState(0);
  const [contador2, setContador2] = useState(0);

  // Referencia con useRef() para asociar una variable con un elemento
  // del DOM
  const miRef = useRef();

  function incrementar1() {
    setContador1((contador) => ++contador);
  }
  function incrementar2() {
    setContador2((contador) => ++contador);
  }

  /**
   * useEffect
   */

  // /**
  //  * ? Caso1: Ejecutar SIEMPRE un snippet de codigo
  //  * Cada vez que haya un cambio en el estado del componente
  //  * se ejecuta aquello que este dentro del useEffect()
  //  */

  // useEffect(() => {
  //   console.log('CAMBIO EN EL ESTADO DEL COMPONENTE');
  //   console.log('Mostrando referencia a elemento del DOM:');
  //   console.log(miRef);
  // });

  // /**
  //  * ? Caso2: Ejecutar solo cuando cambia contador1
  //  * Cada vez que haya un cambio en contador 1 se ejecuta lo que hay
  //  * en useEffect
  //  */

  // useEffect(() => {
  //     console.log('CAMBIO EN EL ESTADO DEL COMPONENTE');
  //     console.log('Mostrando referencia a elemento del DOM:');
  //     console.log(miRef);
  // }, [contador1]);

  /**
   * ? Caso3: Ejecutar solo cuando cambia contador1
   * Cada vez que haya un cambio en contador 1 o contador 2
   * se ejecuta lo que hay en useEffect
   */

  useEffect(() => {
      console.log('CAMBIO EN EL ESTADO DEL COMPONENTE');
      console.log('Mostrando referencia a elemento del DOM:');
      console.log(miRef);
  }, [contador1, contador2]);

  return (
    <div>
      <h1>*** Ejemplo de useState(), useRef() y useEffect() ***</h1>
      <h2>CONTADOR1: {contador1}</h2>
      <h2>CONTADOR2: {contador2}</h2>
      {/* Elemento referenciado */}
      <h4 ref={miRef}>Ejemplo de elemento referenciado</h4>
      {/* Botones para cambiar los contadores */}
      <div>
        <button onClick={incrementar1}>Incrementar contador 1</button>
        <button onClick={incrementar2}>Incrementar contador 2</button>
      </div>
    </div>
  );
};

export default Ejemplo2;
