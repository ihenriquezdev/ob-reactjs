/**
 * Ejemplo de uso del hook useState
 *
 * Crear un componente de tipo funcion y acceder a su estado
 * privado a traves de un hook y, ademas, poder modificarlo
 */

import React, { useState } from 'react';

const Ejemplo1 = () => {
  // valor inicial para un contador
  const valorInicial = 0;

  //valor inicial para una persona
  const personaInicial = {
    nombre: 'Juan',
    email: 'example@mail.com'
  };

  /**
   * Queremos que el VALOR inicial y PERSONA incial sean parte
   * del estado del componente para asi poder gestionar su cambio
   * y que este se vea reflejado en la vista del componente
   */

  const [contador, setContador] = useState(valorInicial);
  const [persona, setPersona] = useState(personaInicial);
  /**
   * Funcion para actualizar el estado privado que contiene el contador
   */
  function incrementarContador() {
    // ? funcionParaCambiar(nuevoValor)
    setContador((contador) => ++contador);
  }

  /**
   * Funcion para actualizar estado de persona en el componente
   */
  function actualizarPersona() {
    setPersona({
      nombre: 'Pepe',
      email: 'pepe@mail.com'
    });
  }

  return (
    <div>
      <h1>*** Ejemplo de useState() ***</h1>
      <h2>CONTADOR: {contador}</h2>
      <h2>DATOS DE LA PERSONA:</h2>
      <h3>NOMBRE: {persona.nombre}</h3>
      <h4>EMAIL: {persona.email}</h4>
      {/* Bloque de botones para actualizar el estado de componente */}
      <button onClick={incrementarContador}>Increamentar contador</button>
      <button onClick={actualizarPersona}>Actualizar persona</button>
    </div>
  );
};

export default Ejemplo1;
