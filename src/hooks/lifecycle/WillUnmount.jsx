/**
 * Ejemplo de uso del metodo componentWillUnmount poara componente clase
 * y ejemplo de uso de hook para componente funcional, cuando desaparece
 */

import React, { Component, useEffect } from 'react';

export default class WillUnmount extends Component {
  componentWillUnmount() {
    console.log('Comportamiento antes de que el componente desaparezca');
  }
  render() {
    return (
      <div>
        <h1>WillUnmount</h1>
      </div>
    );
  }
}

export const WillUnmountHook = () => {
  useEffect(() => {
    // aqui no ponemos nada
    return () => {
      console.log('Comportamiento antes de que el componente desaparezca');
    };
  }, []);
  return (
    <div>
      <h1>WillUnmount</h1>
    </div>
  );
};
