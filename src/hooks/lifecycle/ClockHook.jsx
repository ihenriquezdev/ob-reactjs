import React, { useEffect, useState } from 'react';
import '../../styles/clock.scss';

const initialState = {
  fecha: new Date(),
  edad: 0,
  nombre: 'Ignacio',
  apellido: 'Henriquez'
};

const ClockHook = () => {
  const [data, setData] = useState(initialState);

  const tick = () =>
    setData((prevState) => ({
      ...prevState,
      edad: ++prevState.edad,
      fecha: new Date()
    }));

  useEffect(() => {
    const intervalID = setInterval(() => tick(), 1000);

    return () => {
      clearInterval(intervalID);
    };
  }, []);
  return (
    <div>
      <h2>
        Hora Actual:
        {data.fecha.toLocaleTimeString()}
      </h2>
      <h3>
        {data.nombre} {data.apellido}
      </h3>
      <h1>Edad: {data.edad}</h1>
    </div>
  );
};

export default ClockHook;
