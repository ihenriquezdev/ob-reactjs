/***
 * Ejemplo de use de metodo componente DidUpdate en componente clase
 * y uso de Hook en componente funcional
 */

import React, { Component, useEffect } from 'react';

export default class DidUpdate extends Component {
  componentDidUpdate() {
    console.log(
      'Comportamiento cuando el componente recibe nuevos props o cambios en su estado'
    );
  }
  render() {
    return (
      <div>
        <h1>DidUpdate</h1>
      </div>
    );
  }
}

export const DidUpdateHook = () => {
  useEffect(() => {
    console.log(
      'Comportamiento cuando el componente recibe nuevos props o cambios en su estado'
    );
  });
  return (
    <div>
      <h1>DidUpdate</h1>
    </div>
  );
};
