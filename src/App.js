import logo from './logo.svg';
import './App.css';
// import Clock from './hooks/lifecycle/Clock';
import ClockHook from './hooks/lifecycle/ClockHook';
// import GreetingStyled from './components/pure/greetingStyled';
// import Greeting from './components/pure/greeting';
// import Greetingf from './components/pure/greetingF';
// import TaskListComponent from './components/container/task_list';
// import Ejemplo1 from './hooks/Ejemplo1';
// import Ejemplo2 from './hooks/Ejemplo2';
// import Ejemplo3 from './hooks/Ejemplo3';
// import Ejemplo4 from './hooks/Ejemplo4';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* Componente propio Greeting.jsx */}
        {/* <Greeting name={"Martín"}></Greeting> */}
        {/* Componente de ejemplo funcional */}
        {/* <Greetingf name="Martín"></Greetingf> */}
        {/* Componente de Listado de Tareas */}
        {/* <TaskListComponent></TaskListComponent> */}
        {/* Ejemplos de uso de HOOKS */}
        {/* <Ejemplo1 /> */}
        {/* <Ejemplo2 /> */}
        {/* <Ejemplo3 /> */}
        {/* Todo lo que hay aqui, es tratado como props.children */}
        {/* <Ejemplo4 nombre="Ignacio">
          <h3>Contenido del children</h3>
        </Ejemplo4> */}
        {/* <GreetingStyled name="ignacio" /> */}
        {/* <Clock /> */}
        <ClockHook/>
      </header>
    </div>
  );
}

export default App;
