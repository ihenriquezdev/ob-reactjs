/**
 * Ejemplo de componente de tipo clase que dispone de los
 * metodos de ciclo de vida
 */

import React, { Component } from 'react';

class LifeCycleExample extends Component {
  constructor(props) {
    super(props);
    console.log('CONSTRUCTOR: Cuando se instancia el componente');
  }

  componentWillMount() {
    console.log('[componentWillMount]: Antes del montaje del componente');
  }

  componentDidMount() {
    console.log(
      '[componentDidMount]: Justo al acabar el montaje del componente, antes de pintarlo'
    );
  }

  componentWillReceiveProps(nextProps) {
    console.log('[componentWillReceiveProps]: Si recibe nuevas props');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(
      '[shouldComponentUpdate]: Controla si el componente debe o no actualizarse'
    );
    // return true;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('[componentWillUpdate]: Justo antes de actualizarse');
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('[componentDidUpdate]: Cuando ha sido actualizado');
  }

  componentWillUnmount() {
    console.log('[componentWillUnmount]: Cuando desaparece el componente');
  }

  render() {
    return <div></div>;
  }
}

export default LifeCycleExample;
