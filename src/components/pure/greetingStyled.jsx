import React, { useState } from 'react';

// Definiendo estilos en constantes

// ? Usuario logueado
const loggedStyle = {
  color: 'white'
};

//? Usuario no logueado
const unloggedStyle = {
  color: 'tomato',
  fontWeight: 'bold'
};

const GreetingStyled = (props) => {
  const [logged, setLogged] = useState(false);

  const messageToUser = logged ? (
    <p>Hola, {props.name}</p>
  ) : (
    <p>Please login</p>
  );
  const greetingUser = () => <p>Hola, {props.name}</p>;
  const pleaseLogin = () => <p>Please login</p>;
  return (
    <div style={logged ? loggedStyle : unloggedStyle}>
      {messageToUser}
      {logged ? greetingUser() : pleaseLogin()}
      <button
        onClick={() => {
          console.log('boton pulsado');
          setLogged(!logged);
        }}
      >
        {logged ? 'Logout' : 'Login'}
      </button>
    </div>
  );
};

export default GreetingStyled;
